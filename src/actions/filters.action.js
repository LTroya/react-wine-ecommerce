export const setSubLevelsQuery = (subLevelsQuery = {}) => ({
  type: 'SET_SUB_LEVELS_QUERY',
  subLevelsQuery
});

export const setSubLevel = (subLevel = {}) => ({
    type: 'SET_SUB_LEVEL',
    subLevel,
});

export const setAvailableStatus = (availableStatus) => ({
    type: 'SET_AVAILABLE_STATUS',
    availableStatus,
});

export const setStartPrice = (startPrice) => ({
    type: 'SET_START_PRICE',
    startPrice,
});

export const setEndPrice = (endPrice) => ({
    type: 'SET_END_PRICE',
    endPrice,
});

export const setQuantity = (quantity) => ({
    type: 'SET_QUANTITY',
    quantity,
});

export const sortByPrice = () => ({
    type: 'SORT_BY_PRICE',
});

export const sortByAvailableStatus = () => ({
    type: 'SORT_BY_AVAILABLE_STATUS',
});

export const sortByStock = () => ({
    type: 'SORT_BY_STOCK',
});
