export const addProducts = (products) => ({
    type: 'ADD_PRODUCTS',
    products,
});

export const addProduct = (product) => ({
    type: 'ADD_PRODUCT',
    product,
});

export const editProduct = (id, updates) => ({
    type: 'EDIT_PRODUCT',
    id,
    updates,
});

export const removeProducts = () => ({
    type: 'REMOVE_PRODUCTS',
});

export const removeProduct = (id) => ({
    type: 'REMOVE_PRODUCT',
    id,
});
