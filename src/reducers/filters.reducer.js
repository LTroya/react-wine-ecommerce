// Los productos se deben filtrar por:
//   - Disponibilidad
//   - Rango de precios
//   - Cantidad en stock

// Los productos se pueden ordenar por:
//   - Precio
//   - Disponibilidad
//   - Cantidad (Stock)

export const filtersReducerDefaultState = {
    subLevelsQuery: {},
    subLevel: {},
    availableStatus: true,
    startPrice: '',
    endPrice: '',
    quantity: '',
    sortBy: 'price',
};

const filtersReducer = (state = filtersReducerDefaultState, action) => {
    switch (action.type) {
        case 'SET_SUB_LEVELS_QUERY':
            return {...state, subLevelsQuery: action.subLevelsQuery};
        case 'SET_SUB_LEVEL':
            return { ...state, subLevel: action.subLevel };
        case 'SET_AVAILABLE_STATUS':
            return { ...state, availableStatus: action.availableStatus };
        case 'SET_START_PRICE':
            return { ...state, startPrice: action.startPrice };
        case 'SET_END_PRICE':
            return { ...state, endPrice: action.endPrice };
        case 'SET_QUANTITY':
            return { ...state, quantity: action.quantity };
        case 'SORT_BY_PRICE':
            return { ...state, sortBy: 'price'};
        case 'SORT_BY_AVAILABLE_STATUS':
            return { ...state, sortBy: 'available'};
        case 'SORT_BY_STOCK':
            return { ...state, sortBy: 'quantity'};
        default:
            return state;
    }
};

export default filtersReducer;
