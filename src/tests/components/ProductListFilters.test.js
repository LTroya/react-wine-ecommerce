import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import { ProductListFilters } from '../../components/ProductListFilters';
import { filtersReducerDefaultState } from '../../reducers/filters.reducer';

let wrapper,
    setStartPrice,
    setEndPrice,
    setAvailableStatus,
    setQuantity,
    setSortByPrice,
    setSortByAvailable,
    setSortByQuantity,
    filters = filtersReducerDefaultState;

beforeEach(() => {
    setStartPrice = jest.fn();
    setEndPrice = jest.fn();
    setAvailableStatus = jest.fn();
    setQuantity = jest.fn();
    setSortByPrice = jest.fn();
    setSortByAvailable = jest.fn();
    setSortByQuantity = jest.fn();
    wrapper = shallow(<ProductListFilters filters={filters}
                                          setSortByQuantity={setSortByQuantity}
                                          setSortByAvailable={setSortByAvailable}
                                          setSortByPrice={setSortByPrice}
                                          setQuantity={setQuantity}
                                          setAvailableStatus={setAvailableStatus}
                                          setEndPrice={setEndPrice}
                                          setStartPrice={setStartPrice}/>);
});

test('should render ProductListFilters correctly', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should allow valid amount on startPrice', () => {
    const value = '1234';
    wrapper.find('Input').at(0).simulate('change', {
        target: { value, name: 'startPrice' },
    });
    expect(setStartPrice).toHaveBeenCalledWith(value);
});

test('should not allow invalid amount on startPrice', () => {
    const value = 'Not allowed';
    wrapper.find('Input').at(0).simulate('change', {
        target: { value },
    });
    expect(setStartPrice).toHaveBeenCalledTimes(0);
});

test('should allow valid amount on endPrice', () => {
    const value = '1234';
    wrapper.find('Input').at(1).simulate('change', {
        target: { value, name: 'endPrice' },
    });
    expect(setEndPrice).toHaveBeenCalledWith(value);
});

test('should not allow invalid amount on endPrice', () => {
    const value = 'Not allowed';
    wrapper.find('Input').at(1).simulate('change', {
        target: { value },
    });
    expect(setStartPrice).toHaveBeenCalledTimes(0);
});

test('should handle quantity change', () => {
    const value = '123';
    wrapper.find('Input').at(2).simulate('change', {
        target: { value },
    });
    expect(setQuantity).toHaveBeenCalledWith(value);
});

test('should handle available status change', () => {
    wrapper.find('Input').at(3).simulate('change', {
        target: { checked: false },
    });
    expect(setAvailableStatus).toHaveBeenCalledWith(false);
});

test('should handle sortBy change to price', () => {
    const value = 'price';
    wrapper.find('Input').at(4).simulate('change', {
        target: { value },
    });
    expect(setSortByPrice).toHaveBeenCalled();
});

test('should handle sortBy change to available', () => {
    const value = 'available';
    wrapper.find('Input').at(4).simulate('change', {
        target: { value },
    });
    expect(setSortByAvailable).toHaveBeenCalled();
});

test('should handle sortBy change to quantity', () => {
    const value = 'quantity';
    wrapper.find('Input').at(4).simulate('change', {
        target: { value },
    });
    expect(setSortByQuantity).toHaveBeenCalled();
});
