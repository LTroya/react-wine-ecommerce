import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import {Header} from '../../components/Header';

let wrapper;

beforeEach(() => {
    wrapper = shallow(<Header cartCount={5}/>);
});

test('should render Header correctly', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should handle toggle nav-bar', () => {
    wrapper.find('NavbarToggler').simulate('click');
    expect(wrapper.state('isOpen')).toBe(true);
});
